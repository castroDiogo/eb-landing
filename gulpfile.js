var gulp = require('gulp');
var postcss = require('gulp-postcss');

gulp.task('css', function () {
    return gulp.src('assets/*.css')
        .pipe( postcss([
            require('precss'),
            //require('cssnext'),
            require('autoprefixer'),
            require('postcss-normalize'),
            //require('postcss-mixins'),
            require ('postcss-caralho'),
            require('cssnano')
        ]) )
        .pipe( gulp.dest('dist') );
});

gulp.task('corre', function(){
   gulp.watch('assets/*.css', ['css']);
});

